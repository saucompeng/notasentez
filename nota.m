function  [a]=nota(note,vurus)
 
switch note
%% native notalar
case {'A'} %la
frekans=440;
case {'a'} %la#
frekans=466.16;
case {'B'} %si
frekans=493.92;
case {'C'} %do
frekans=523.28;
case {'c'} %do#
frekans=554.40;
case {'D'} %re
frekans=587.36;
case {'d'} %re#
frekans=622.24;
case {'E'} %mi
frekans=659.28;
case {'F'} %fa
frekans=698.48;
case {'f'} %fa#
frekans=740;
case {'G'} %sol
frekans =784;
%% oktav yukari
case {'H'} %la
frekans=2*440;
case {'h'} %la#
frekans=2*466.16;
case {'I'} %si
frekans=2*493.92;
case {'J'} %do
frekans=2*523.28;
case {'j'} %do#
frekans=2*554.40;
case {'K'} %re
frekans=2*587.36;
case {'k'} %re#
frekans=2*622.24;
case {'L'} %mi
frekans=2*659.28;
case {'M'} %fa
frekans=2*698.48;
case {'m'} %fa#
frekans=2*740;
case {'N'} %sol
frekans=2*784;
%% oktav asagi
case {'O'} %la
frekans=440/2;
case {'o'} %la#
frekans=466.16/2;
case {'P'} %si
frekans=493.92/2;
case {'Q'} %do
frekans=523.28/2;
case {'q'} %do#
frekans=554.40/2;
case {'R'} %re
frekans=587.36/2;
case {'r'} %re#
frekans=622.24/2;
case {'S'} %mi
frekans=659.28/2;
case {'T'} %fa
frekans=698.48/2;
case {'t'} %fa#
frekans=740/2;
case {'U'} %sol
frekans =784/2;
%% diger durumlar
otherwise
disp('Yok boyle bisey')
frekans=0;
end
%% Belirlenen frekansda ve vurus suresinde sinus
%  sinyali fonksiyonun cikisina veriliyor.
a=sin(2*pi*frekans*(0:1/16383:vurus));