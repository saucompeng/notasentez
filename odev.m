clear
close all
% 1-2 olculerin notalari
olcu12=['L','K','F','G'];  % YOmi YOre NAfa NAsol
zaman12=[1/4 1/4 1/2 1/2]; % notalarin vurus degerleri
% 3-4 olculerin notalari
olcu34=['J','I','D','E'];  % YOdo YOsi NAre NAmi
zaman34=[1/4 1/4 1/2 1/2]; % notalarin vurus degerleri
% 5-6 olculerin notalari
olcu56=['I','H','C','E'];  % YOsi YOla NAdo NAmi
zaman56=[1/4 1/4 1/2 1/2]; % notalarin vurus degerleri
% 7-8 olculerin notalari
olcu78=['H']; % YO la
zaman78=[1]; % notalarin vurus degerleri
%vurus degerleri artarak bitiyor.
 
%% olcu 1-2
aa1(1,:)=nota(olcu12(1),zaman12(1));
aa1(2,:)=nota(olcu12(2),zaman12(2));

bb1(1,:)=nota(olcu12(3),zaman12(3));
bb1(2,:)=nota(olcu12(4),zaman12(4));

aa2(1,:)=nota(olcu34(1),zaman34(1));
aa2(2,:)=nota(olcu34(2),zaman34(2));

bb2(1,:)=nota(olcu34(3),zaman34(3));
bb2(2,:)=nota(olcu34(4),zaman34(4));

aa3(1,:)=nota(olcu56(1),zaman56(1));
aa3(2,:)=nota(olcu56(2),zaman56(2));

bb3(1,:)=nota(olcu56(3),zaman56(3));
bb3(2,:)=nota(olcu56(4),zaman56(4));

aa4(1,:)=nota(olcu78(1),zaman78(1));

 
%% dizilerin hepsini cal.
tumdizi=[           nota(olcu12(1),zaman12(1)) nota(olcu12(2),zaman12(2)) nota(olcu12(3),zaman12(3)) nota(olcu12(4),zaman12(4))];
tumdizi=[tumdizi    nota(olcu34(1),zaman34(1)) nota(olcu34(2),zaman34(2)) nota(olcu34(3),zaman34(3)) nota(olcu34(4),zaman34(4))];
tumdizi=[tumdizi    nota(olcu56(1),zaman56(1)) nota(olcu56(2),zaman56(2)) nota(olcu56(3),zaman56(3)) nota(olcu56(4),zaman56(4))];
tumdizi=[tumdizi    nota(olcu78(1),zaman78(1))];
soundsc(tumdizi,30000);
%bitti :)

plot(tumdizi);    % tumdizinin spektrumunu cizdir.
grid on;
title('Yapay Sinyalin Spektrumu')

